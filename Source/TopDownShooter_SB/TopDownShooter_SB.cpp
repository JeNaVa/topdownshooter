// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooter_SB.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDownShooter_SB, "TopDownShooter_SB" );

DEFINE_LOG_CATEGORY(LogTopDownShooter_SB)
 