
#pragma once

#include "kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	SlowWalk_State UMETA(DisplayName = "SlowWalk State"),
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	float AimSpeed = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	float WalkSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	float Run = 900.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	float SlowWalkSpeed = 200.f;
	
};



UCLASS()
class TOPDOWNSHOOTER_SB_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};