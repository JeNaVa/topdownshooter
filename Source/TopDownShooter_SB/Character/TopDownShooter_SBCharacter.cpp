// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooter_SBCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATopDownShooter_SBCharacter::ATopDownShooter_SBCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 90.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}


void ATopDownShooter_SBCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}


void ATopDownShooter_SBCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooter_SBCharacter::InputAxisX);

	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooter_SBCharacter::InputAxisY);
}

void ATopDownShooter_SBCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooter_SBCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownShooter_SBCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1,0,0), AxisX);
	AddMovementInput(FVector(0,1,0), AxisY);


	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		float YawRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, YawRotation, 0.0f)));
	}
}

void ATopDownShooter_SBCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.Run;
		break;
	case EMovementState::SlowWalk_State:
		ResSpeed = MovementInfo.SlowWalkSpeed;
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	
}

void ATopDownShooter_SBCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

FVector ATopDownShooter_SBCharacter::GetCharacterDirection()
{
	FVector Direction = UKismetMathLibrary::MakeVector(AxisX, AxisY, 0);
	UKismetMathLibrary::Vector_Normalize(Direction, 0.0001f);
	return Direction;
}



void ATopDownShooter_SBCharacter::DoADash()
{
	if (bCanDoDash)
	{
		FVector CharacterDirection = GetCharacterDirection() * DashPower;
		LaunchCharacter(CharacterDirection, false, false);
		bCanDoDash = false;
	}
}

void ATopDownShooter_SBCharacter::RunForwardOnlyCheck()
{
	FVector CharacterDir = GetCharacterDirection();
	FVector CharacterLookDir = GetCapsuleComponent()->GetForwardVector();
	
	if (CharacterDir.Equals(CharacterLookDir, 0.2f))
		bIsCanRun = true;
	else
		bIsCanRun = false;
}

float ATopDownShooter_SBCharacter::StaminaLogic()
{
	if (bIsRunning)
	{
		if (stamina >= 0.f)
		{
			stamina -= 1.f;
		}
		else
		{
			bIsRunning = false;
			bIsCanRun = false;
			ChangeMovementState(EMovementState::Walk_State);
		}
	}
	else
	{
		if (stamina < 500.f)
		{
			stamina += 2;
		}
	}
	return stamina;
}

