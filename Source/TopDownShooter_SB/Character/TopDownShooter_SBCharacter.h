// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDownShooter_SB/FuncLibrary/Types.h"
#include "TopDownShooter_SBCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownShooter_SBCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownShooter_SBCharacter();

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	float DashPower = 1500;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	float stamina = 500.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	bool bCanDoDash = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	bool bIsCanRun = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	bool bIsAiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	bool bIsWalking = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =  "Movement")
	bool bIsRunning = false;

	
	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAxisY(float Value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	//Tick func
	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState(EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void DoADash();
	

	UFUNCTION(BlueprintCallable)
	FVector GetCharacterDirection();

	UFUNCTION(BlueprintCallable)
	void RunForwardOnlyCheck();

	UFUNCTION(BlueprintCallable)
	float StaminaLogic();
	
	
};

