// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopDownShooter_SBGameMode.generated.h"

UCLASS(minimalapi)
class ATopDownShooter_SBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDownShooter_SBGameMode();
};



