// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooter_SBGameMode.h"
#include "TopDownShooter_SBPlayerController.h"
#include "TopDownShooter_SB/Character/TopDownShooter_SBCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDownShooter_SBGameMode::ATopDownShooter_SBGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownShooter_SBPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
} 